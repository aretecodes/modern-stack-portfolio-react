# modern-stack-portfolio-react

[![Deploy to now](https://deploy.now.sh/static/button.svg)](https://deploy.now.sh/?repo=https://github.com/aretecode/modern-stack-portfolio-react)

See it live at [jameswiens.dev](https://jameswiens.dev)

![](https://user-images.githubusercontent.com/4022631/56465430-a3cd1b00-63b1-11e9-8f6f-1f7613a6552e.png)

This is the [react](https://reactjs.org) part of the [modern stack portfolio](https://github.com/aretecode/modern-stack-web-portfolio)
also see the [graphql part](https://github.com/aretecode/modern-stack-portfolio-graphql)
